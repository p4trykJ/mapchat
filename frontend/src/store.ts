import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const DIVI_LOGIN = {
  api_key: 'ODQyMQ.pPeAfuB75TqZrWRfwsJVwt3PPfM',
  id: 'M0X',
  status: 2,
  token: 'NTkwNzQ4.LwzxwS5J4EjHBQoTTDf_2-dFEwI',
};
const DIVI_USERS = {
  email: 'kl@gmail.com',
  first_name: 'Jan Kowalski',
  id_users: 'M0X',
  lang: 'pl',
  last_name: 'Admin',
  phone: null,
  position: null,
  status: 2,
  user_type: 'individual',
};


export default new Vuex.Store({
  state: {
    isLogged: false,
    token: localStorage.getItem('token') === null ? '' : localStorage.getItem('token'),
    user: {
      id: localStorage.getItem('user_id') === null ? '' : localStorage.getItem('user_id'),
      email: '',
      username: '',
      description: '',
      lang: '',
    },
    profileData: {},
    users: [],
  },
  mutations: {
    setIsLogged(state, isLogged) {
      state.isLogged = isLogged;
    },
    setToken(state, token) {
      state.token = token.slice();
      localStorage.setItem('token', token.slice());
    },
    setUser(state, data) {
      state.user.id = data.id_users;
      state.user.email = data.email;
      state.user.username = data.first_name;
      state.user.lang = 'pl';
      state.user.description = 'Description person about osoba';
      state.isLogged = true;
      localStorage.setItem('user_id', data.id_users);
    },
    setProfileData(state, profileData) {
      state.profileData = profileData;
    },
    logOut(state) {
      state.token = '';
      state.user.email = '';
      state.user.username = '';
      state.isLogged = false;
      localStorage.removeItem('token');
    },
    setUsers(state, users) {
      state.users = users;
    },
  },
  actions: {
    checkToken(ctx: any) {
      return new Promise((resolve, reject) => {
        if (ctx.state.token.length === 0 || ctx.state.user.id.length === 0) {
          reject();
        } else {
          // Mockowanie logowania z DIVI
          ctx.commit('setToken', DIVI_LOGIN.token);
          ctx.commit('setUser', DIVI_USERS);
          ctx.commit('setIsLogged', true);
          resolve();
        }
      });
    },
    logIn(ctx, payload) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          const token = 'tk';
          ctx.commit('setToken', DIVI_LOGIN.token);
          ctx.commit('setUser', DIVI_USERS);
          localStorage.setItem('token', token);
          resolve('Logged');
        }, 2000);
      });
    },
  },
  getters: {
    getIsLogged(state) {
      return state.isLogged;
    },
    getUser(state) {
      return state.user;
    },
    getProfileData(state) {
      return state.profileData;
    },
    getUsers(state) {
      return state.users;
    },
  },
});
