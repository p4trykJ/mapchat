import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/Login.vue';
import store from '@/store';
import MapGlobal from './views/MapGlobal.vue';
import Chats from './views/Chats.vue';
import Chat from './views/Chat.vue';
import Profile from './views/Profile.vue';
import Settings from './views/Settings.vue';


Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/login',
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/map',
      name: 'map',
      component: MapGlobal,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/profile/:id',
      name: 'profileUser',
      // props: route => {
      //   u
      // },
      props: true,
      component: Profile,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/chats',
      name: 'chats',
      component: Chats,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/chat',
      redirect: '/chats',
    },
    {
      path: '/chat/:id',
      name: 'chat',
      component: Chat,
      props: true,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings,
      meta: {
        requiresAuth: true,
      },
    },
  ],
});

router.beforeEach((to, from, next) => {

  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!store.getters.getIsLogged) {
      store.dispatch('checkToken')
        .then(() => {
          next();
        })
        .catch(() => {
          next({ name: 'login' });
        });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
