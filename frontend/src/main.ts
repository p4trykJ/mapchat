import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import 'ol/ol.css';
import VueSocketIOExt from 'vue-socket.io-extended';
import io from 'socket.io-client';

const socket = io('http://138.68.151.177', {
  path: '/api/socket.io',
  forceNew: true,
  transports: ['polling', 'websocket'],
  transportOptions: {
    polling: {
      extraHeaders: {
        'X-Access-Token': 'MQ.xtLdNnbJXI3en5TbnfqB9LPRBBs',
        'Content-Type': 'application/json',
      },
    },
  },
});
Vue.use(VueSocketIOExt, socket);
// @ts-ignore
import VueGeolocation from 'vue-browser-geolocation';
Vue.use(VueGeolocation);


declare global {
  interface Window { vm: Vue; }
}

window.vm = new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
