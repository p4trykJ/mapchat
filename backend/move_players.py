PORT=1410

from random import randint
from time import sleep

import requests
from socketIO_client import SocketIO

USERS = [
    {"username": "Wojciech Suchodolski", "password": "admin0"},
    {"username": "Miloslava Nekvasilová", "password": "admin1"},
    {"username": "Helmut Kleiner", "password": "admin2"}
]

USERS_COORDS = [
    [20.897369384765625, 52.15898024846712],
    [9.348058, 51.709711],
    [16.265782, 49.460660]
]

def fn(*args):
    print("eventName", args)

def get_token(url, usernumber):
    response = requests.post(url, json=USERS[usernumber])
    token = response.json()['token']
    return token

while True:
    usernumber = randint(0,2)

    x = USERS_COORDS[usernumber][0] + 0.001 * randint(10,500)
    y = USERS_COORDS[usernumber][1] + 0.001 * randint(10,500)

    token = get_token('http://localhost:{}/login'.format(PORT), usernumber)
    payload = {
        "lng": x,
        "lat": y
    }

    with SocketIO("http://0.0.0.0", PORT,
    headers = {
        "X-Access-Token": token,
        "Content-Type": "application/json"
    },
    cookies = {},
    proxies = {}) as socketIO:
        
        socketIO.emit("put/users/geom", payload, fn);
        # sleep(1)



