from datetime import datetime, timedelta

from flask import (abort, current_app, g, jsonify, redirect, render_template,
                   request)
from flask_cors import cross_origin

from . import mod_auth
from .token import create_new_token, id_to_token, get_token
from ..utils.utils import abort_with_code
from ..models.models import PasswordField, Users

@mod_auth.route('/login', methods=['POST'])
@cross_origin(supports_credentials=True)
def login():
    #Logowanie
    result = sign_in(request.get_json(force=True), False)

    if result:
        resp = jsonify({'token':result})
        resp.set_cookie('accesstoken', result)
        return resp
    else:
        abort_with_code(403, "Brak dostępu")

@mod_auth.route('/logout')
@cross_origin(supports_credentials=True)
def logout():
    """ Wylogowanie """
    sign_out()
    return 'ok'


def sign_in(payload, only_admin=True, without_request=False):
    """ Logowanie do aplikacji """
    try:
        u = Users.select().where(Users.username==payload.get('username')).get()

        #zahashowanie hasła z payloadu statyczną metodą klasy PasswordField (models.py)
        password_correct = u.password == PasswordField.hash_value(payload['password'])

        if not password_correct:
            abort_with_code( 403, "Błąd, niepoprawne hasło" )
        
        token = create_new_token(u, datetime.now()+timedelta(hours=12), without_request = without_request)
        #Na razie wyłączenie sprawdzania połączenia z QGIS
        #if u.sid and not only_admin:
            #Użytkownik zalogowany przez SocketIO (nie dotyczy logowania HTTP)
        #    return False

        return id_to_token(token.id)
    except Users.DoesNotExist as e:
        return

def sign_out():
    token = get_token(request)
    if token is None:
        return
    token.valid_to = datetime.now()
    token.save()