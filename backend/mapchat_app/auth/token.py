from datetime import datetime, timedelta

from flask import current_app, request
from itsdangerous import BadSignature, URLSafeSerializer

from ..models.models import Tokens, Users

def id_to_token(db_id):
    """
    Utworzenie tokena w postaci ciągu znaków, na podstawie ID wiersza
    @para int db_id ID utworzonego tokena
    @return string token Gotowy token do wysłania
    """
    zs = URLSafeSerializer(current_app.config['SECRET_KEY'])
    return zs.dumps(db_id)

def token_to_id(a_token):
    """
    Odtworzenie ID tokena w bazie na podstawie ciągu znaków
    Weryfikuje, czy token jest prawidłowo podpisany
    @pram string a_token Token
    @return int ID z tabeli tokens
    """
    zs = URLSafeSerializer(current_app.config['SECRET_KEY'])
    try:
        return zs.loads(a_token)
    except BadSignature:
        return None

#Sprawdzanie tokenów

def get_token_from_request(req):
    """
    Wyciągnięcie access tokena z obiektu Request
    @param object req Request
    @return int ID tokena w bazie danych
    """
    token = req.headers.get('X-Access-Token') #w pierwszej kolejności z nagłówka
    # print(req)
    if token is None:
        token = req.args.get('token') #w drugiej z query stringa
    if token is None:
        token = req.cookies.get('accesstoken') #wreszcie z ciastek
    if token is None:
        print('none :(', req.args)
        return None
    else:
        return token_to_id(token)

def get_token_from_db(tokenid):
    """
    Wyciągnięcie tokena z bazy danych
    @param int tokenid ID tokena (odszyfrowany)
    @return object Obiekt klasy Tokens
    """
    the_token = None
    if the_token is None:
        try:
            the_token = (Tokens
                .select(Tokens, Users)
                .join(Users)
                .where(Tokens.id == tokenid).get())
        except Tokens.DoesNotExist: #brak takiego tokena w bazie
            #print 'Token not found in DB, id was %s' % str(tokenid)
            return None
    if the_token.valid_to < datetime.now(): #token wygasł
        #print 'Token expired, id was %s' % str(tokenid)
        return None
    elif the_token.valid_to < datetime.now() + timedelta(minutes=30):
        #token wymaga odświeżenia
        #print 'Token extended'
        the_token.valid_to = datetime.now() + timedelta(hours=2)
        the_token.save()
    return the_token

def get_token(req):
    """
    Funkcja łącząca dwie poprzednie
    @param object req Request
    @return object Obiekt klasy Tokens, lub None gdy nie znaleziono
    """
    token = get_token_from_db(get_token_from_request(req))
    try:
        #Najwyraźniej PostgreSQL traktuje klucze jako łańcuch znaków
        #więc trzeba je skonwertować na int
        # MM: Klucze w JSON są zawsze stringami
        token.projects = { int(k):v for k,v in token.projects.items() }
    except AttributeError:
        pass
    return token

def create_new_token(user, valid_to, without_request=False):
    """ Stworzenie nowego tokena podczas logowania """
    
    new_token_data = {
        'user': user,
        'valid_from': datetime.now(),
        'valid_to': valid_to,
        'app': 'web',
    }
    return Tokens.create(**new_token_data)
