from flask import jsonify, request
import json

from ..models.models import Users, database
from ..utils.decorators import login_required
from ..socketer import socketio, emit_data
from ..utils.utils import abort_with_code
from ..utils.users import get_user
from . import mod_users
from ..auth.token import get_token

@socketio.on('get/users')
@login_required('socket')
def users(data={}):
    users = []
    rs = Users.select().dicts()
    for user in rs:
        user.pop('password')
        users.append(user)
    return {'data':users}


@socketio.on('put/users/geom')
@login_required('socket')
def put_users_geom(data={}):
    user = get_user()
    user.lng = data['lng']
    user.lat = data['lat']
    user.save()

    response = {
        "user_id": user.id,
        "lng":user.lng,
        "lat":user.lat
    }
    emit_data(response, 'geom_updated')

@socketio.on('put/users/description')
@login_required('socket')
def put_users_description(data):
    user = get_user()
    user.description = data['description']
    user.save()

    response = {
        'user_id':user.id,
        'new_description':user.description
    }
    emit_data(response, 'description_updated')