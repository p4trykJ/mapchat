import json, requests
from time import sleep
from flask import jsonify
from datetime import datetime
from ..models.models import Messages, Chats
from . import mod_messages
from ..socketer import socketio, emit_data
from ..utils.decorators import login_required
from ..utils.utils import abort_with_code
from ..utils.users import get_user

@socketio.on('get/messages')
@login_required('socket')
def getMessageForChatId(data={}):
    # pobierz wszystkie wiadomości z danego czatu
    user_id = data.get('user_id', None)
    if user_id is None:
        abort_with_code(404, "nieprawidłowe ID")
    
    try:
        chat = Chats.get(
            user1=get_user().id,
            user2=int(user_id)
        )
    except Chats.DoesNotExist:
        abort_with_code(400, "Błędne ID")

    messages = Messages.select().where(
        Messages.chat_id == chat.id
    ).dicts()
    result = []

    for m in messages:
        m['message_id'] = m.pop('id')
        m['owner'] = m.pop('user')
        m['created_date'] = m.pop('created_date').strftime( '%d-%m-%Y %H:%M' )
        result.append(m)

    return {'data':result}

@socketio.on('post/message')
@login_required('socket')
def postMessage(data={}):
    owner = get_user()
    content = data.get('content')
    user_id = data.get('user_id')
    user_language = data.get('user_language')
    owner_language  = owner.languages

    if data.get('translate'):
        response = requests.get('https://api.mymemory.translated.net/get?q={}&langpair={}|{}'.format(content, owner_language, user_language))
        content = response.json()['responseData']['translatedText']

    if not content or not user_id:
        abort_with_code(400, 'Nie podano wiadomości lub użytkownika')

    try:
        chat = Chats.get(
            (Chats.user1==user_id & Chats.user2==owner.id) |
            (Chats.user1==owner.id & Chats.user2==user_id)
        )
    except Chats.DoesNotExist:
        chat = Chats.create(
            user1 = owner.id,
            user2 = user_id
        ).dicts()

    message = Messages.create(
        user=owner.id,
        chat_id=chat.id,
        content=content
    )

    data = message.content

    if user_id != 1:
        response = {
            "content": "Sorry, no active users to chat with. Busy hacking.",
            "user_id": 1,
            "created_date": datetime.now().strftime( '%d-%m-%Y %H:%M' )
        }
        sleep(1.5)
        emit_data(response, 'response_message')
    
    return {'data':data}