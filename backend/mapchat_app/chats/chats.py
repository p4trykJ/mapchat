# from flask import jsonify
from flask_cors import cross_origin
from peewee import InternalError

from . import mod_chats
from ..utils.decorators import login_required
from ..utils.users import get_user
from ..utils.utils import abort_with_code
from ..socketer import socketio
from ..models.models import Users, Chats, Messages

@socketio.on('get/chats')
@login_required('socket')
def socket_chats(data={}):
    return {"data": get_chats()}

@socketio.on('post/chats')
@login_required('socket')
def socket_post_chats(data={}):
    post_chats(**data)

def get_chats():
    user = get_user()
    chats = Chats.select().where(
        (
            Chats.user1==user.id |
            Chats.user2==user.id
        )
    ).dicts()

    result = []

    for c in chats:
        if int(c['user1']) == user.id:
            other_user = c['user2']
        else:
            other_user = c['user1']

        c['title'] = Users.get(Users.id==other_user).username

        last_messages = Messages.select(Messages.content).where(
            Messages.chat_id==c['id']
        ).order_by(Messages.created_date.desc()).limit(1)
        if len(last_messages) > 0:
            c['subtitle'] = last_messages[0].content
        else:
            c['subtitle'] = 'No messages'

        result.append(c)

    return result

def post_chats(user1, user2, **kwargs):
    if user1 is None or user2 is None:
        abort_with_code(400, "Nie podano prawidłowych ID użytkowników")

    if user1==user2:
        abort_with_code(400, "To samo ID dla obu użytkowników")

    if not Chats.select().where(
        (Chats.user1==user1 & Chats.user2==user1) |
        (Chats.user2==user1 & Chats.user1==user2)
    ):
        try:
            c = Chats.create(
                user1=user1,
                user2=user2
            )
            return c.id
        except peewee.InternalError:
            abort_with_code(400, "Podano nieprawidłowe ID użytkowników")
    else:
        abort_with_code(400, "Już istnieje czat dla takiej pary użytkowników")