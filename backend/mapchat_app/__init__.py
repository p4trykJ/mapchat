import os
import traceback
import json
import numpy as np

# import eventlet
# eventlet.monkey_patch(os=False)

from flask import Flask, redirect, render_template, g, request, json as flask_json
from flask_socketio import SocketIO, emit
from flask_cors import CORS

from .socketer import socketio
from .config import config
from .utils.exceptions import AbortException
from .models.models import *
from .auth.token import get_token

cors = CORS()

def create_app(config_name='testing'):
    ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
    os.chdir(ROOT_DIR)
    app = Flask(__name__)

    app.config.from_object(config[config_name])
    try:
        #Próba załadowania ustawień z lokalnej konfiguracji
        from .local_config import config as loc_config
        app.config.from_object(loc_config.get(config_name, {}))
    except ImportError:
        pass
    socketio.init_app(app, async_handlers=False, cors_allowed_origins="*")

    cors.init_app( app )

    from .status import mod_status
    from .auth import mod_auth
    from .chats import mod_chats
    from .users import mod_users
    from .messages import mod_messages
    app.register_blueprint(mod_status)
    app.register_blueprint(mod_auth)
    app.register_blueprint(mod_users)
    app.register_blueprint(mod_messages)

    @app.before_request
    def before():
        g.current_token = get_token(request)
        g.session = None

    @app.after_request
    def after(response):
        if not database.is_closed():
            database.close()
        return response

    #Tworzenie bazy danych
    if not os.path.isfile("main.db"):
        createTables()

    @socketio.on_error_default
    def default_error_handler(e):
        """ Obsługa błędów podczas wykonywania żądań SocketIO """
        if type(e) is AbortException:
            emit('error', e.__dict__)
            return
        print( '------------------------------')
        print(( 'ERROR while executing request', '\n'))
        print(( 'ENDPOINT:', request.event["message"]))
        print(( 'DATA:', request.event["args"], '\n'))
        print(( traceback.format_exc()))
        print( '------------------------------')
        #Zwrócenie informacji o błędzie
        emit('uncaught_error', {'error':str(e)})

    return app


def createTables():
    with database:
        database.create_tables([Users, Tokens, Chats, Messages])

        rand_num = np.random.beta(0.2,1)
        rand_num2 = np.random.beta(0.2,1)
        Users.create(
            username='Wojciech Suchodolski',
            password='admin0',
            languages='pl',
            description='Pozdrawiam w tę piękną pogodę :)',
            lng=21. + rand_num,
            lat=52. + rand_num2                 
        )

        rand_num = np.random.beta(0.2,1)
        rand_num2 = np.random.beta(0.2,1)
        Users.create(
            username='Kate Springfield',
            password='admin1',
            languages='en',
            description='Holidays in Germany',
            lng=9. + rand_num,
            lat=51. + rand_num2                 
        )

        rand_num = np.random.beta(0.2,1)
        rand_num2 = np.random.beta(0.2,1)
        Users.create(
            username='Helmut Kleiner',
            password='admin2',
            languages='de',
            description='Deutsche Autos sind die besten',
            lng=16. + rand_num,
            lat=49. + rand_num2                 
        )

        chat12 = Chats.create(
                user1=1,
                user2=2
            )

        chat13 = Chats.create(
            user1=1,
            user2=3
            )

        Messages.create(
            user=1,
            chat_id=chat12,
            content="Hi!"
        )
        Messages.create(
            user=1,
            chat_id=chat12,
            content="How are you?"
        )
        Messages.create(
            user=2,
            chat_id=chat12,
            content="Im great, thanks! And how are you?"
        )
        Messages.create(
            user=1,
            chat_id=chat12,
            content="I'm fine. I can see on the map that you are in Germany. How is it?"
        )
        Messages.create(
            user=1,
            chat_id=chat13,
            content="Hallo Helmut!"
        )
        Messages.create(
            user=2,
            chat_id=chat13,
            content="Hallo, Wojtek! Ich habe im Moment keine Zeit dafür. Ich werde dir später schreiben!"
        )
        Messages.create(
            user=1,
            chat_id=chat13,
            content="Ok! Bis später dann!"
        )