import base64, pyscrypt
from datetime import datetime
from playhouse.sqlite_ext import SqliteExtDatabase
from peewee import (Field, Model, AutoField, TextField, ForeignKeyField, DateTimeField,
    CharField, PrimaryKeyField, FloatField)
from flask import current_app

database = SqliteExtDatabase('main.db', field_types={'password':'text'})

class BaseModel(Model):
    class Meta:
        database = database

class PasswordHash(str):
    """ Fakowa klasa do rozpoznawania wartości z bazy danych
    i zapobiegania ich ponownemu zahashowaniu """
    pass

class PasswordField(Field):
    ''' Pole do przechowywania hasła '''
    db_field = 'password'

    @classmethod
    def hash_value(cls,  value):
        ''' Pole do przechowywania hasła '''
        if isinstance(value, str):
            value = value.encode('utf-8')
        return base64.b64encode(pyscrypt.hash(value, b'43f0c18696ea56f476da7ffcc42b9b582ca8c99f3b1ae314f211c68057d6f87baaaa67ae561dbfdb',
            N = 1024, r = 1, p = 1, dkLen = 64)).decode("UTF-8")
    
    def db_value(self, value):
        if type( value ) is PasswordHash:
            return value
        return self.hash_value(value)

    def python_value(self, value):
        return PasswordHash( value )

class Users(BaseModel):
    ''' Tabela użytkowników '''
    id = PrimaryKeyField()
    username = CharField(unique=True)
    password = PasswordField()
    languages = TextField()
    description = TextField()
    lng = FloatField()
    lat = FloatField()
    class Meta:
        db_table = 'users'

class Tokens(BaseModel):
    ''' Tabela tokenów '''
    id = PrimaryKeyField()
    user = ForeignKeyField(db_column='user', model=Users, related_name='tokens_users', to_field='id', null=True)
    valid_from = DateTimeField()
    valid_to = DateTimeField()
    class Meta:
        db_table = 'tokens'

class Chats(BaseModel):
    ''' Tabela czatów '''
    id = PrimaryKeyField()
    user1 = ForeignKeyField(db_column='user1', model=Users, related_name='chats_users', to_field='id', null=True)
    user2 = ForeignKeyField(db_column='user2', model=Users, related_name='chats_users', to_field='id', null=True)
    class Meta:
        db_table = 'chats'

class Messages(BaseModel):
    ''' Tabela wiadomości '''
    id = PrimaryKeyField()
    user = ForeignKeyField(db_column='user', model=Users, related_name='messages_users', to_field='id', null=True)
    chat_id = ForeignKeyField(db_column='chat_id', model=Chats, related_name='messages_chats', to_field='id', null=True)
    content = TextField()
    created_date = DateTimeField(default=datetime.now)
    class Meta:
        db_table = 'messages'