#!/usr/bin/python

from flask import g
from flask_socketio import SocketIO#, emit

from .utils.users import get_user


#Ze względu na konflikt importów definicja SocketIO musi znajdować się w osobnym pliku
socketio = SocketIO()

def emit_error(message, rollback=True, data={}):
    """ Wysłanie błędu """
    data['error'] = message
    socketio.emit('error', data)
    return data

def emit_changes(operation, data, featureId=0):
    #Wysłanie aktualizacji do wszystkich
    user = get_user()
    try:
        session = g.session
    except AttributeError:
        session = None
    #if user.mod_water or user.mod_sewer:
    #    raise ModerationException('moderation error', data)
    socketio.emit('data_changed', {
        'meta' : {
            'operation' : operation,
            'featureId' : featureId,
            'username' : user.name,
            'session' : session
        },
        'data' : data
    }, broadcast=True)
    return data

def emit_data(data, endpoint='data'):
    """ Wysłanie dowolnej informacji do wszystkich użytkowników """
    socketio.emit(endpoint, data, broadcast=True)
    return data
