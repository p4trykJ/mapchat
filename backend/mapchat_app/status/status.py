
from flask import jsonify
from flask_cors import cross_origin

from . import mod_status
from ..utils.decorators import login_required
from ..socketer import socketio, emit_data
from ..models.models import Users
from ..utils.users import get_user

@mod_status.route('/status')
@login_required('json')
@cross_origin(supports_credentials=True)
def status():
    # Users.create(username='iwan', password='dddd', languages='ru')
    return jsonify("hello")

@socketio.on('status')
@login_required('socket')
def socket_status(data={}):
    print('data', data)
    print(get_user())
    emit_data({"test": "test"}, "status_test_emit")
    return {"data": "twoja stara"}