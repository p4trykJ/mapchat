from functools import wraps
from flask import redirect, g, request
from flask_socketio import disconnect

from ..utils.utils import abort_with_code
from ..auth.token import get_token

def login_required(request_type='json'):
    """ Dekorator sprawdzający czy użytkownik jest zalogowany przed wysłaniem odpowiedzi
    request_type: 'html' dla stron HTML, 'json' dla REST, 'socket' dla SocketIO """
    def wrap(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if request_type == 'socket':
                g.current_token = get_token(request)
            if g.current_token is None:
                if request_type == 'json':
                    abort_with_code(403)
                    return
                elif request_type == 'socket':
                    disconnect()
                    return {}
                else:
                    #Odpowiedź HTML
                    return redirect('/')
            return f(*args, **kwargs)
        return decorated_function
    return wrap