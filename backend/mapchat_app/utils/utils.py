from flask import abort, make_response, jsonify, request

from .exceptions import AbortException

def is_rest_request():
    """ Zwraca True jeżeli działamy w kontekście żądania Flask
    w przeciwnym wypadku (np socketIO) zwraca False """
    return request.endpoint is not None

def abort_with_code(code, msg='Błąd', as_json=True, **kwargs):
    if is_rest_request():
        #REST
        if as_json:
            abort(make_response(jsonify(message=msg, **kwargs), code ) )
        else:
            abort(code)
    else:
        #SocketIO
        result = {'code': code, 'error':msg, **kwargs}
        raise AbortException(**result)