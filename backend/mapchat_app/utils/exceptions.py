class ApplicationException(Exception):
    #Klasa bazowa dla błędów aplikacji
    pass


class AbortException(ApplicationException):
    """
    Wyjątek używany przy przerywaniu requestów SocketIO
    """
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)