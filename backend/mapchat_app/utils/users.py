from flask import g, session, current_app
from ..models.models import Users
# from .utils import abort_with_code

def get_user():
    """ Pobranie aktualnego użytkownika """
    try:
        user = g.current_token.user
    except AttributeError:
        return
    if user is None:
        try:
            user = Users.get(id=session.get('uid'))
            g.user = user
        except Users.DoesNotExist:
            return
    return user