


class Config:
    SECRET_KEY = b'43f0c18696ea56f476da7ffcc42b9b582ca8c99f3b1ae314f211c68057d6f87baaaa67ae561dbfdb'

    DBNAME = ''
    DBUSER = ''
    DBPASS = ''
    DBHOST = ''
    DBPORT = ''

    SERVER_ADDRESS = '0.0.0.0'
    SERVER_PORT = 5000


class TestingConfig(Config):
    TESTING = True
    DEBUG=True

config = {
    'testing' : TestingConfig
    }
