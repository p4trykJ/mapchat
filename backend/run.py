#!/usr/bin/python

from mapchat_app import create_app
from mapchat_app.socketer import socketio

if __name__ == '__main__':
    app = create_app('testing')
    socketio.run(app, host=app.config['SERVER_ADDRESS'], port=app.config['SERVER_PORT'])
